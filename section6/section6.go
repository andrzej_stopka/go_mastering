package section6

import (
	"errors"
	"fmt"
	"log"
	"time"
)

func Intro() {
	result, err := divide(4, 2)
	if err != nil {
	  log.Fatal(err)
	}
	fmt.Printf("Result %f\n", result)

	_, err = divide(4, 0)
	if err != nil {
	  log.Printf("Error: %s\n", err)
	}
}

func PanicAndRecover() {
	panicAndRecover()
	fmt.Println("Starting flight booking process...")
	bookFlight()
	fmt.Println("Flight booking process continues after recovery...")
}

func panicAndRecover() {
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("Recovered from error while booking:", r)
		}
	}()
	fmt.Println("Attempting to book a flight")

	panic("Failed to book the flight due to system error!")
}

func divide(a, b float64) (float64, error) {
	if b == 0 {
		return 0, errors.New("division by zero is not allowed")
	}
	return a / b, nil
}

func bookFlight() {
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("Recovered from:", r)
		}
	}()
	fmt.Println("Attempting to book a flight...")
	time.Sleep(2 * time.Second)
	panic("Flight not available")
}