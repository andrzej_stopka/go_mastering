package advanced

import (
	"fmt"
)

func Pointers() {
	var n = 10
	var p = &n

	fmt.Printf("Type of pointer: %T\n", p)
	fmt.Printf("Address: %v\n", p)
	fmt.Printf("Value: %v\n", *p)

	var a int = 50
	var b *int = &a

	fmt.Println(a)
	fmt.Println(b)
	*b = 100
	fmt.Println(a)
	fmt.Println(*b)
	var a2 int = 10
	fmt.Printf("Original value: %d\n", a2)
	modify(&a2)
	fmt.Printf("Modified value: %d\n", a2)

	p2 := &Point{X: 5, Y: 3}
	fmt.Println(*p2)
	p2.ChangeCoordinates(1, 2)
	fmt.Println(*p2)

	numbers := []int{1, 2, 3, 4, 5}
	fmt.Println("Original slice:", numbers)
	modifySlice(&numbers)
	fmt.Println("Modified slice:", numbers)
}

type Point struct {
	X int
	Y int
}

func (p *Point) ChangeCoordinates(xNew, yNew int) {
	p.X = xNew
	p.Y = yNew
}

func modify(x *int) {
	*x = 50
}

func modifySlice(slicePtr *[]int) {
	*slicePtr = append(*slicePtr, 100)
}

