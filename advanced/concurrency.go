package advanced

import (
	"fmt"
	"sync"
	"time"
)

func Concurrency() {
	go processOrder("Buy 100 shares of AAPL")
	go func(order string) {
		fmt.Println("Processing order:", order)
	}("Buy 100 shares of AAPL")

	fmt.Println("Main function continues executing...")

	orders := []string{"Buy 100 shares of AAPL", "Sell 50 shares of GOOGL", "Buy 200 shares of MSFT"}
	ch := make(chan string, len(orders))

	for _, order := range orders {
		go func(o string) {
			ch <- processOrder(o)
			}(order)
		}

	for i := 0; i < len(orders); i++ {
		fmt.Println(<-ch)
	}
	var waitgroup sync.WaitGroup

	orders2 := []string{"Buy 100 shares of AAPL", "Sell 50 shares of GOOGL", "Buy 200 shares of MSFT"}

	waitgroup.Add(len(orders))

	for _, order := range orders2 {
		go func(o string) {
			defer waitgroup.Done()
			fmt.Println("Processing order:", o)
			}(order)
		}
		waitgroup.Wait()
		fmt.Println("All orders have been processed!")
		time.Sleep(1 * time.Second)
}

func processOrder(order string) string{
	return fmt.Sprintf("Processing order: %s", order)
}