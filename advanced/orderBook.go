package advanced

import (
	"fmt"
	"sync"
)

type OrderBook struct {
	mx sync.Mutex
	orders []string
}

func OrderBookMain() {
	var wg sync.WaitGroup
	ob := OrderBook{}

	orders := []string{"Buy 100 shares of AAPL", "Sell 50 shares of GOOGL", "Buy 200 shares of MSFT"}

	wg.Add(len(orders))

	for _, order := range orders {
		go ob.addOrder(&wg, order)
	}

	wg.Wait()
	fmt.Println("All orders have been added:", ob.orders)
}

func (ob *OrderBook) addOrder(wg *sync.WaitGroup, order string) {
	ob.mx.Lock()
	defer ob.mx.Unlock()
	defer wg.Done()

	ob.orders = append(ob.orders, order)
	fmt.Println("Added order: ", order)
}