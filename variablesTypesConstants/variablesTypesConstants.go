package variablesTypesConstants

import (
	"fmt"
	"strings"
)

func DataTypes() {
	var age int = 20
	fmt.Println("Age:", age)

	var pi float64 = 3.14159
	fmt.Println("Value of Pi:", pi)

	name := "John"
	fmt.Println("Name:", name)

	if score := 85; score >= 50 {
		fmt.Println("Passed with score:", score)
	} else {
		fmt.Println("Failed with score:", score)
	}
}

func Variables() {
	var age int = 25
	var name string = "John"
	fmt.Println("Age:", age)
	fmt.Println("Name:", name)

	city := "New York"
	year := 2024

	fmt.Println("City:", city)
	fmt.Println("Year:", year)
}

func ConstantsAndIOTA() {
	const (
		CheckingAccount = iota
		SavingAccount
		BusinessAccount
	)
	fmt.Println("Checking Account:", CheckingAccount)
	fmt.Println("Saving Account:", SavingAccount)
	fmt.Println("Business Account:", BusinessAccount)

	const (
		_ = iota
		KB = 1 << (10 * iota)
		MB
		GB
		TB
	)

	fmt.Println("Size of KB:", KB)
	fmt.Println("Size of MB:", MB)
	fmt.Println("Size of GB:", GB)
	fmt.Println("Size of TB:", TB)
}

func String() {
	var message1 = "Hello World"
	message2 := "Hello Golang"
	fmt.Println(message1)
	fmt.Println(message2)

	message3 := "I love Go Programming"
	fmt.Println(message3)

	name := "Programmer"
	fmt.Printf("%c\n", name[0])
	fmt.Printf("%c\n", name[3])
	fmt.Printf("%c\n", name[8])

	message4 := "Welcome to Programmer World"
	stringLength := len(message4)
	fmt.Println("Length of a string is:", stringLength)

	message5 := "I love"
	message6 := "Go programming"

	result := message5 + " " + message6
	fmt.Println(result)

	string1 := "admin 01"
	string2 := "admin Pro"
	string3 := "admin 03"

	fmt.Println(strings.Compare(string1, string2))
	fmt.Println(strings.Compare(string2, string3))
	fmt.Println(strings.Compare(string1, string3))

	email := "Apple@bank.com"
	containsBank := strings.Contains(email, "bank")
	fmt.Println(containsBank)

	oldEmail := "Apple@Citibank.com"
	newEmail := strings.Replace(oldEmail, "Citi", "Chase", 1)
	fmt.Println(newEmail)

	profileName := "BankUserProfile"
	fmt.Println(strings.ToUpper(profileName))
	fmt.Println(strings.ToLower(profileName))

	accountDetails := "AccountID-12345;Balance-1000"
	details := strings.Split(accountDetails, ";")
	fmt.Println(details)
}