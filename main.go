package main

import (
	"mastering_go/basics/introduction"
	"mastering_go/basics/variablesTypesConstants"
	"mastering_go/basics/controlStructures"
	"mastering_go/basics/functionsAndMethods"
	"mastering_go/basics/compositeTypes"
	"mastering_go/basics/errorHandling"
	"mastering_go/basics/advanced"
)


func main() {
	introduction.HelloWorld()
	variablesTypesConstants.DataTypes()
	variablesTypesConstants.Variables()
	variablesTypesConstants.ConstantsAndIOTA()
	variablesTypesConstants.String()
	controlStructures.ConditionalStatements()
	controlStructures.Loops()
	controlStructures.SwitchAndSelect()
	functionsAndMethods.DeclaringFunctions()
	functionsAndMethods.ReturnValues()
	functionsAndMethods.AnonymousFunctions()
	functionsAndMethods.Closures()
	functionsAndMethods.Methods()
	compositeTypes.Array()
	compositeTypes.Slice()
	compositeTypes.Map()
	errorHandling.Intro()
	errorHandling.PanicAndRecover()
	errorHandling.Defer()
	advanced.Pointers()
	advanced.Concurrency()
	advanced.OrderBookMain()
	advanced.Imports()

}
