package errorHandling

import (
	"fmt"
	"time"
)

func PanicAndRecover() {
	panicAndRecover()
	fmt.Println("Starting flight booking process...")
	bookFlight()
	fmt.Println("Flight booking process continues after recovery...")
}


func panicAndRecover() {
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("Recovered from error while booking:", r)
		}
	}()
	fmt.Println("Attempting to book a flight")

	panic("Failed to book the flight due to system error!")
}

func bookFlight() {
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("Recovered from:", r)
		}
	}()
	fmt.Println("Attempting to book a flight...")
	time.Sleep(2 * time.Second)
	panic("Flight not available")
}