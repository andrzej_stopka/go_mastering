package section4

import (
	"fmt"
	"strings"
)

func Methods() {
	user := User2{Name: "Alice"}
	fmt.Println(user.greet())

	user.upperName()
	fmt.Println(user.Name)

	userPointer := &user
	userPointer.setName("Bob")
	fmt.Println(user.Name)
}

type User2 struct {
	Name string
}

func (u User2) greet() string {
	return "Hello " + u.Name
}

func (u User2) upperName() {
	u.Name = strings.ToUpper(u.Name)
}

func (u *User2) setName(name string) {
	u.Name = name
}