package functionsAndMethods

import (
	"fmt"
)

func Closures() {
	getUser := newUser("FistName1", "LastName1")
	fmt.Println(getUser())
	deposit := newAccount(1000.0)
	fmt.Println(deposit(100.0))
	fmt.Println(deposit(-50.0))
	operate := accountOperations()
	fmt.Println(operate(100.0))
	fmt.Println(operate(-50.0))
	updateLastName := updateUserProfile2("FirstName2")
	fmt.Println(updateLastName("LastName2"))
	service := newBankServices()
	fmt.Println(service.Transfer(1000.0, 200.0))
}

type User struct {
	FirstName string
	LastName string
}

type BankAccount struct {
	Balance float64
}

type BankServices struct {
	transfer func(float64, float64) float64
}

func newUser(firstName, lastName string) func() User {
	user := User{FirstName: firstName, LastName: lastName}
	return func() User {
		return user
	}
}

func newAccount(initialBalance float64) func(float64) float64 {
	account := BankAccount{Balance: initialBalance}
	return func(amount float64) float64 {
		account.Balance += amount
		return account.Balance
	}
}

func accountOperations() func(float64) float64 {
	balance := 0.0
	return func(amount float64) float64 {
		balance += amount
		return balance
	}
}

func updateUserProfile2(firstName string) func(string) string {
	return func(newLastName string) string {
		return "Updated last name for " + firstName + ": " + newLastName
	}
}

func newBankServices() *BankServices {
	s := &BankServices{}
	s.transfer = func(from, to float64) float64 {
		return from - to
	}
	return s
}

func (s *BankServices) Transfer(from, to float64) float64 {
	return s.transfer(from, to)
}

