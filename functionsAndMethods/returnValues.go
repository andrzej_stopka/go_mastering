package functionsAndMethods

import (
	"fmt"
	"errors"
)

func ReturnValues() {
	change, err :=  percentChange(100.0, 120.0)
	if err != nil {
	  	fmt.Println("An error occurred:", err.Error())
		return
	}
	fmt.Println("The percentage change is:", change)

	quotient, remainder := divide(10, 3)
	fmt.Printf("The quotient is: %d, and the remainder is: %d\n", quotient, remainder)

	fmt.Println(greet("Alice"))
	a, b := 5, 10

	a, b = swap(a, b)
	fmt.Printf("Swapped values: a = %d, b = %d\n", a, b)
}

func percentChange(oldPrice, newPrice float64) (float64, error) {
	if oldPrice == 0.0 {
		return 0, errors.New("old price cannot be zero")
	}

	return ((newPrice - oldPrice) / oldPrice) * 100, nil
}

func divide(a, b int) (quo, rem int) {
	quo = a/b
	rem = a%b
	return
}

func greet(name string) string {
	return "Hello, " + name + "!"
}

func swap(x, y int) (int, int) {
	return y, x
}