module mastering_go/basics

go 1.21.4

require (
	github.com/go-sql-driver/mysql v1.7.1
	github.com/nsf/termbox-go v1.1.1
)

require github.com/mattn/go-runewidth v0.0.9 // indirect
