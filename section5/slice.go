package section5

import (
	"fmt"
	"unsafe"
)

func Slice() {
	var stockPricesArray [3]float64 = [3]float64{150.25, 152.85, 149.50}
	var stockPricesSlice []float64 = []float64{150.25, 152.85, 149.50}

	fmt.Println(stockPricesArray)
	fmt.Println(stockPricesSlice)

	users := []string{"Name_1", "Name_2", "Name_3"}
	users = append(users, "Name_4")
	fmt.Println("Appended User Slice:", users)

	stockPricesSlice2 := make([]float64, 3, 5)
	fmt.Println("Slice with make - Length:", len(stockPricesSlice2))
	fmt.Println("Slice with make - Capacity:", cap(stockPricesSlice2))

	usersInternal := []string{"Name_1", "Name_2"}
	sliceInternals := (*SliceStruct)(unsafe.Pointer(&usersInternal))
	fmt.Println("Slice internals:", sliceInternals)

	stockPrices := []float64{150.25, 152.85, 149.50, 151.75, 153.30}
	subset := stockPrices[1:4]
	fmt.Println("Sliced Slice:", subset)

	usersSubset := users[1:3]
	usersSubset[0] = "Name_7"
	fmt.Println("Modified users subset:", users)
}

type SliceStruct struct {
	ArrayPointer unsafe.Pointer
	Length int
	Capacity int
}