package section4

import (
	"fmt"
)

func DeclaringFunctions() {
	original := 5
	result := square(original)
	fmt.Printf("The original value is %d and the squared value is %d\n", original, result)

	original2 := 5
	squareWithPointers(&original2)
	fmt.Printf("The original value after the square operation is %d\n", original2)

	flight1 := Flight{price: 150}
	flight2 := Flight{price: 200}

	fmt.Println(BookFlights(flight1, flight2))

	flight3 := Flight{price: 100}
	flight4 := Flight{price: 250}
	flight5 := Flight{price: 300}

	fmt.Println(BookFlights(flight1, flight2, flight3, flight4, flight5))
}

type Flight struct {
	price int
}

func BookFlights(flights ...Flight) int {
	totalCost := 0
	for _, flight := range flights {
		totalCost += flight.price
	}
	return totalCost
}

func square(num int) int {
	num *= num
	return num
}

func squareWithPointers(ptr *int) {
	*ptr *= *ptr
}

