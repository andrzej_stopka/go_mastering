package compositeTypes

import (
	"fmt"
)

func Array() {
	var stockPrices [5]float64
	stockPrices = [5]float64{142.0, 150.5, 147.8, 155.0, 143.5}
	fmt.Println(stockPrices)

	userProfiles := [...]struct {
		Name string
		Email string
	}{
		{"FirstName1 LastName1", "firstname1.lastname1@example.com"},
		{"FirstName2 LastName2", "firstname2.lastname2@example.com"},
	}
	fmt.Println(userProfiles)

	stockSymbols := [...]string{"AAPL", "GOOGL", "AMZN"}
	fmt.Println(stockSymbols)

	stocks := [3]string{"Apple", "Google", "Amazon"}
	fmt.Println(stocks[0])

	var userAges [5]int
	userAges[0] = 25
	userAges[2] = 30
	fmt.Println(userAges)

	stockPrices2 := [3]float64{142.0, 150.5, 147.8}
	stockPrices2[2] = 148.0
	fmt.Println(stockPrices2)
	users := [3]string{"John", "Jane", "Alice"}
	totalUsers := len(users)
	fmt.Println("Total number of users:", totalUsers)

	for _, symbol := range stockSymbols {
		fmt.Println(symbol)
	}

	userProfiles2 := [2][2]struct {
		Name string
		Email string
	}{
		{{"FirstName1 LastName1", "firstname1.lastname1@example.com"}, {"FirstName2 LastName2", "firstname2.lastname2@example.com"}},
		{{"FirstName3 LastName3", "firstname3.lastname3@example.com"}, {"FirstName4 LastName4", "firstname4.lastname4@example.com"}},
	}
	for _, row := range userProfiles2 {
		for _, user := range row {
			fmt.Println(user.Name, user.Email)
		}
	}
}
