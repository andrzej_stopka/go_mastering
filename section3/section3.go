package section3

import (
	"fmt"
	"time"
)

func ConditionalStatements() {
	num := 10

	if num % 2 == 0 {
		fmt.Println(num, "is even")
	}

	if num == 100 {
		fmt.Println("Japan")
	} else if num == 200 {
		fmt.Println("China")
	} else {
		fmt.Println("Germany")
	}

	if num := 10; num % 2 == 0 {
		fmt.Println(num, "is even")
	}
}

func Loops() {
	str := "Hello"

	for i := 0; i < len(str); i++ {
		fmt.Printf("Character at index %d is %c\n", i, str[i])
	}

	for i, v := range str {
		fmt.Printf("Character at index %d is %c\n", i, v)
	}

	m := map[string]string{
		"one": "First",
		"two": "Second",
		"three": "Third",
	}

	for k, v := range m {
		fmt.Printf("Key: %s, Value: %s\n", k, v)
	}
}

func SwitchAndSelect() {
	bankingOperations := make(chan string)

	go func() {
		time.Sleep(1 * time.Second)
		bankingOperations <- "Deposit completed"
	}()

	select {
	case msg := <-bankingOperations:
		fmt.Println(msg)
	case <-time.After(3 * time.Second):
		fmt.Println("Operation timeout!")
	}

	accountStatus := "Active"

	switch accountStatus {
	case "Active":
		fmt.Println("Your account is active")
		fallthrough
	case "Inactive":
		fmt.Println("Your account is inactive") 
	default:
		fmt.Println("Your account status is unknown")
	}
}