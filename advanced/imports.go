package advanced

import (
	"fmt"
	"mastering_go/basics/calculator"
	"github.com/nsf/termbox-go"
)

func Imports() {
	err := termbox.Init()
	if err != nil {
	  panic(err)
	}
	defer termbox.Close()

	result := calculator.Add(5, 3)
	fmt.Println(result)
}