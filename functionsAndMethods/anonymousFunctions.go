package functionsAndMethods

import (
	"fmt"
)

func AnonymousFunctions() {
	func() {
		fmt.Println("Hello from an anonymous function")
	}()

	func(firstName, lastName string) {
		fmt.Printf("User profile: %s %s\n", firstName, lastName)
	}("First name", "Last name")

	displayBalance := func(accountHolder string, balance float64) {
		fmt.Printf("%s's balance: $%.2f\n", accountHolder, balance)
	}
	displayBalance("First Name Last Name", 1000.50)

	func(firstName, lastName, email string) {
		fmt.Printf("User: %s %s, Email: %s\n", firstName, lastName, email)
	}("First Name", "Last Name", "firstname.lastname@example.com")

	deposit := func(amount float64) float64 {
		fmt.Printf("Deposited $%.2f\n", amount)
		return amount
	}

	newBalance := processTransaction(500.50, deposit)
	fmt.Printf("New Balance: $%.2f\n", newBalance)

	update := updateUserProfile()
	newProfile := update("FirstName LastName's updated profile")
	fmt.Println(newProfile)
}

func processTransaction(amount float64, operation func(float64) float64) float64 {
	return operation(amount)
}

func updateUserProfile() func(string) string {
	profile := "First Name Last Name's profile"
	return func(update string) string {
		profile = update
		return profile
	}
}