package section5

import (
	"fmt"
)

func Map() {
	userProfile := map[string]string{"FirstName_1": "Name_1",
									"Email_1": "Name_1@example.com",
									"Phone_1": "123-456-7890"}
	fmt.Println(userProfile)

	fmt.Println("FirstName:", userProfile["FirstName_1"])
	fmt.Println("Email:", userProfile["Email_1"])
	fmt.Println("Phone:", userProfile["Phone_1"])

	userProfile["Email_1"] = "Name_2@example.com"
	fmt.Println(userProfile)

	userProfile["LastName_1"] = "Name_2"
	fmt.Println(userProfile)

	delete(userProfile, "Phone_1")
	fmt.Println(userProfile)

	for key, value := range userProfile {
		fmt.Printf("%s: %s\n", key, value)
	}

	bankAccount := make(map[string]float64)
	bankAccount["Name_1"] = 1000.50
	fmt.Println(bankAccount)

	for accountHolder := range bankAccount {
		fmt.Println(accountHolder)
	}

	for _, balance := range bankAccount {
		fmt.Println(balance)
	}
}